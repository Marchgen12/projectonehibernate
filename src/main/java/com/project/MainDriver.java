package com.project;

import com.project.util.HibernateUtil;
import com.projectone.dao.ReimbursementDao;
import com.projectone.dao.ReimbursementStatusDao;
import com.projectone.dao.ReimbursementTypeDao;
import com.projectone.services.ReimbursementServices;
import com.projectone.services.UserServices;

import io.javalin.Javalin;

public class MainDriver {
	
	public static HibernateUtil hUtil = new HibernateUtil();
	public static ReimbursementDao rDao = new ReimbursementDao(hUtil);
	public static ReimbursementStatusDao rsDao = new ReimbursementStatusDao(hUtil);
	public static ReimbursementTypeDao rtDao = new ReimbursementTypeDao(hUtil);
	public static UserServices US = new UserServices();
	public static ReimbursementServices RS = new ReimbursementServices();
	
	public static void main(String[] args) {
		RS.populateStatusRole(hUtil);
		US.populateRole(hUtil);
	
		
		
/*
		Javalin app = Javalin.create(config ->{
			config.addStaticFiles("/frontend");
		}).start(1234);
*/
		
		
		Javalin app = Javalin.create().start(1234);
		
		hUtil.closeSession();
	}
}
