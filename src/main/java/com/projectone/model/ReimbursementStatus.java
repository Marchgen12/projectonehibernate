package com.projectone.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Reimbursement_Status")
public class ReimbursementStatus {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int StatusId;

	@Column(name="Status", nullable=false, length=10)
	private String Status;
	
	public ReimbursementStatus() {
		// TODO Auto-generated constructor stub
	}
	
	public ReimbursementStatus(String status) {
		super();
		Status = status;
	}

	public ReimbursementStatus(int statusId, String status) {
		super();
		StatusId = statusId;
		Status = status;
	}

	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}

	public int getStatusId() {
		return StatusId;
	}

	@Override
	public String toString() {
		return "ReimbursementStatus [StatusId=" + StatusId + ", Status=" + Status + "]";
	}

	
}
