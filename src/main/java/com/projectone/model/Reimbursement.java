package com.projectone.model;

import java.sql.Blob;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="Reimbursement")
public class Reimbursement {

	@Id
	@Column(name="Reimb_id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int ReimbId;
	
	@Column(name="Amount", nullable=false)
	private int Amount;
	
	@Column(name="Submitted", nullable=false)
	@Temporal(TemporalType.DATE)
	private Date Submitted;
	
	@Column(name="Resolved")
	@Temporal(TemporalType.DATE)
	private Date Resolved;
	
	@Column(name="Description", length=250)
	private String Description;
	
	@Lob
	@Column(name="Reciept")
	private Blob Reciept;
	
	@ManyToOne(cascade=CascadeType.ALL, fetch=FetchType.EAGER)
	@JoinColumn(name="ERS_USERS_FK_AUTH", nullable=false)
	private Users AuthorId;
	
	@ManyToOne(cascade=CascadeType.ALL, fetch=FetchType.EAGER)
	@JoinColumn(name="ERS_USERS_FK_RESLVR", nullable=false)
	private Users ResolverId;
	
	@ManyToOne(cascade=CascadeType.ALL, fetch=FetchType.EAGER)
	@JoinColumn(name="ERS_REIMBURSEMENT_STATUS_FK", nullable=false)
	private ReimbursementStatus StatusFK;
	
	@ManyToOne(cascade=CascadeType.ALL, fetch=FetchType.EAGER)
	@JoinColumn(name="ERS_REIMBURSEMENT_TYPE_FK", nullable=false)
	private ReimbursementType TypeFK;

	
	public Reimbursement() {
		// TODO Auto-generated constructor stub
	}
	
	public Reimbursement(int amount, Date submitted, String description, Users AuthorId) {
		super();
		Amount = amount;
		Submitted = submitted;
		Description = description;
	}

	public Reimbursement(int amount, Date submitted, Date resolved, String description, Blob reciept, Users authorId) {
		super();
		Amount = amount;
		Submitted = submitted;
		Resolved = resolved;
		Description = description;
		Reciept = reciept;
		AuthorId = authorId;
	}

	public Reimbursement(int amount, Date submitted, Date resolved, String description, Blob reciept, Users authorId,
			Users resolverId, ReimbursementStatus statusFK, ReimbursementType typeFK) {
		super();
		Amount = amount;
		Submitted = submitted;
		Resolved = resolved;
		Description = description;
		Reciept = reciept;
		AuthorId = authorId;
		ResolverId = resolverId;
		StatusFK = statusFK;
		TypeFK = typeFK;
	}


	public Reimbursement(int reimbId, int amount, Date submitted, Date resolved, String description, Blob reciept,
			Users authorId, Users resolverId, ReimbursementStatus statusFK, ReimbursementType typeFK) {
		super();
		ReimbId = reimbId;
		Amount = amount;
		Submitted = submitted;
		Resolved = resolved;
		Description = description;
		Reciept = reciept;
		AuthorId = authorId;
		ResolverId = resolverId;
		StatusFK = statusFK;
		TypeFK = typeFK;
	}


	public int getAmount() {
		return Amount;
	}


	public void setAmount(int amount) {
		Amount = amount;
	}


	public Date getSubmitted() {
		return Submitted;
	}


	public void setSubmitted(Date submitted) {
		Submitted = submitted;
	}


	public Date getResolved() {
		return Resolved;
	}


	public void setResolved(Date resolved) {
		Resolved = resolved;
	}


	public String getDescription() {
		return Description;
	}


	public void setDescription(String description) {
		Description = description;
	}


	public Blob getReciept() {
		return Reciept;
	}


	public void setReciept(Blob reciept) {
		Reciept = reciept;
	}


	public Users getAuthorId() {
		return AuthorId;
	}


	public void setAuthorId(Users authorId) {
		AuthorId = authorId;
	}


	public Users getResolverId() {
		return ResolverId;
	}


	public void setResolverId(Users resolverId) {
		ResolverId = resolverId;
	}


	public ReimbursementStatus getStatusFK() {
		return StatusFK;
	}


	public void setStatusFK(ReimbursementStatus statusFK) {
		StatusFK = statusFK;
	}


	public ReimbursementType getTypeFK() {
		return TypeFK;
	}


	public void setTypeFK(ReimbursementType typeFK) {
		TypeFK = typeFK;
	}


	public int getReimbId() {
		return ReimbId;
	}

	@Override
	public String toString() {
		return "Reimbursement [ReimbId=" + ReimbId + ", Amount=" + Amount + ", Submitted=" + Submitted + ", Resolved="
				+ Resolved + ", Description=" + Description + ", Reciept=" + Reciept + ", AuthorId=" + AuthorId
				+ ", ResolverId=" + ResolverId + ", StatusFK=" + StatusFK + ", TypeFK=" + TypeFK + "]";
	}
	
	
}
