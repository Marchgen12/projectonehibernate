package com.projectone.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="UserRoles")
public class UserRoles {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int UserRoleId;
	
	@Column(name="User_Role", nullable=false)
	private String UserRole;
	
	@OneToMany(mappedBy="UserRoleId", fetch=FetchType.EAGER)
	private List<Users> UserList = new ArrayList<>();
	
	public UserRoles() {
		// TODO Auto-generated constructor stub
	}
	
	public UserRoles(String userRole) {
		super();
		UserRole = userRole;
	}

	public UserRoles(String userRole, List<Users> userList) {
		super();
		UserRole = userRole;
		UserList = userList;
	}

	public UserRoles(int userRoleId, String userRole, List<Users> userList) {
		super();
		UserRoleId = userRoleId;
		UserRole = userRole;
		UserList = userList;
	}

	public int getUserRoleId() {
		return UserRoleId;
	}

	public String getUserRole() {
		return UserRole;
	}

	public void setUserRole(String userRole) {
		UserRole = userRole;
	}

	public List<Users> getUserList() {
		return UserList;
	}

	public void setUserList(List<Users> userList) {
		UserList = userList;
	}

	@Override
	public String toString() {
		return "UserRoles [UserRoleId=" + UserRoleId + ", UserRole=" + UserRole + ", UserList=" + UserList + "]";
	}
	
	
}
