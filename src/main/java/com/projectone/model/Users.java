package com.projectone.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="Users")
public class Users {
	
	@Id
	@Column(name="User_Id", nullable=false)
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int UserId;
	
	@Column(name="Username", unique=true, nullable=false, length=50)
	private String Username;
	
	@Column(name="Password", nullable=false, length=50)
	private String Password;
	
	@Column(name="First_name", nullable=false, length=100)
	private String Firstname;
	
	@Column(name="Last_name", nullable=false, length=100)
	private String Lastname;
	
	@Column(name="Email", unique=true, nullable=false, length=150)
	private String Email;
	
	@JoinColumn(name="USER_ROLES_FK", nullable=false)
	private int UserRoleId;
	
	@OneToMany(mappedBy="AuthorId", fetch=FetchType.EAGER)
	private List<Reimbursement> ReimbursementList = new ArrayList<>();
	
	public Users() {
		// TODO Auto-generated constructor stub
	}
	
	public Users(String username, String password, String firstname, String lastname, String email, int userRoleId) {
		super();
		Username = username;
		Password = password;
		Firstname = firstname;
		Lastname = lastname;
		Email = email;
		UserRoleId = userRoleId;
	}

	public Users(int userId, String username, String password, String firstname, String lastname, String email,
			int userRoleId, List<Reimbursement> reimbursementList) {
		super();
		UserId = userId;
		Username = username;
		Password = password;
		Firstname = firstname;
		Lastname = lastname;
		Email = email;
		UserRoleId = userRoleId;
		ReimbursementList = reimbursementList;
	}

	public String getUsername() {
		return Username;
	}

	public void setUsername(String username) {
		Username = username;
	}

	public String getPassword() {
		return Password;
	}

	public void setPassword(String password) {
		Password = password;
	}

	public String getFirstname() {
		return Firstname;
	}

	public void setFirstname(String firstname) {
		Firstname = firstname;
	}

	public String getLastname() {
		return Lastname;
	}

	public void setLastname(String lastname) {
		Lastname = lastname;
	}

	public String getEmail() {
		return Email;
	}

	public void setEmail(String email) {
		Email = email;
	}

	public int getUserRoleId() {
		return UserRoleId;
	}

	public void setUserRoleId(int userRoleId) {
		UserRoleId = userRoleId;
	}

	public int getUserId() {
		return UserId;
	}

	public List<Reimbursement> getReimbursementList() {
		return ReimbursementList;
	}

	public void setReimbursementList(List<Reimbursement> reimbursementList) {
		ReimbursementList = reimbursementList;
	}

	@Override
	public String toString() {
		return "Users [UserId=" + UserId + ", Username=" + Username + ", Password=" + Password + ", Firstname="
				+ Firstname + ", Lastname=" + Lastname + ", Email=" + Email + ", UserRoleId=" + UserRoleId
				+ ", ReimbursementList=" + ReimbursementList + "]";
	}
	
	
}
