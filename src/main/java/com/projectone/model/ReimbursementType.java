package com.projectone.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Reimbursement_Type")
public class ReimbursementType {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int TypeId;

	@Column(name="Type", nullable=false, length=10)
	private String Type;
	
	public ReimbursementType() {
		// TODO Auto-generated constructor stub
	}
	
	public ReimbursementType(String type) {
		super();
		Type = type;
	}

	public ReimbursementType(int typeId, String type) {
		super();
		TypeId = typeId;
		Type = type;
	}

	public String getType() {
		return Type;
	}

	public void setType(String type) {
		Type = type;
	}

	public int getTypeId() {
		return TypeId;
	}

	@Override
	public String toString() {
		return "ReimbursementType [TypeId=" + TypeId + ", Type=" + Type + "]";
	}

	
}
