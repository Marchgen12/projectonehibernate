package com.projectone.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.project.util.HibernateUtil;
import com.projectone.model.Reimbursement;

public class ReimbursementDao implements GenericDao<Reimbursement>{
	
	private HibernateUtil hUtil;

	public ReimbursementDao() {
		// TODO Auto-generated constructor stub
	}
		
	public ReimbursementDao(HibernateUtil hUtil) {
		super();
		this.hUtil = hUtil;
	}

	@Override
	public void insert(Reimbursement type) {
		Session ses = hUtil.getSession();
		Transaction tx = ses.beginTransaction();
		
		ses.save(type);
		tx.commit();
	}

	public void update(Reimbursement type) {
		Session ses = hUtil.getSession();
		Transaction tx = ses.beginTransaction();
		
		ses.update(type);
		tx.commit();		
	}

	@Override
	public Reimbursement selectById(int id) {
		Session ses = hUtil.getSession();
		
		Reimbursement reimb = ses.get(Reimbursement.class, id);
		return reimb;
	}
	
	public List<Reimbursement> getListReimbursements(int Userid){
		Session ses = hUtil.getSession();
		
		List<Reimbursement> rList = ses.createQuery("from Reimbursement where ERS_USERS_FK_AUTH = " + Userid + "", Reimbursement.class).list();
		
		if(rList.size() == 0) {
			System.out.println("No reimbursement created");
			return null;
		}
		return rList;
	}
	
	public List<Reimbursement> selectAll(){
		Session ses = hUtil.getSession();
		
		List<Reimbursement> rList = ses.createQuery("from Reimbursement", Reimbursement.class).list();
		if(rList.size() == 0) {
			System.out.println("There is no submitted");
			return null;
		}
		return rList;
	}
	
	public List<Reimbursement> selectByStatus(String status){
		Session ses = hUtil.getSession();
		
		List<Reimbursement> rList = ses.createQuery("from Reimbursement WHERE ERS_REIMBURSEMENT_STATUS_FK = " + 
				"(SELECT StatusId FROM Reimbursement_Status WHERE status= '" + status + "')", Reimbursement.class).list();		
		
		if(rList.size() == 0) {
			System.out.println("No reimbursement with that status");
			return null;
		}		
		return rList;
	}
}
