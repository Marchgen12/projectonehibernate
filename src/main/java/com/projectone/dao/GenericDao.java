package com.projectone.dao;

public interface GenericDao <E> {
	public void insert(E type);
	public E selectById(int id);
}
