package com.projectone.dao;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.project.util.HibernateUtil;
import com.projectone.model.ReimbursementStatus;

public class ReimbursementStatusDao implements GenericDao<ReimbursementStatus>{

	private HibernateUtil hUtil;
	
	public ReimbursementStatusDao() {
		// TODO Auto-generated constructor stub
	}
	
	public ReimbursementStatusDao(HibernateUtil hUtil) {
		super();
		this.hUtil = hUtil;
	}

	@Override
	public void insert(ReimbursementStatus type) {
		Session ses = hUtil.getSession();
		Transaction tx = ses.beginTransaction();
		
		ses.save(type);
		tx.commit();		
	}

	@Override
	public ReimbursementStatus selectById(int id) {
		Session ses = hUtil.getSession();
		
		ReimbursementStatus reimbStat = ses.get(ReimbursementStatus.class, id);
		return reimbStat;
	}
}
