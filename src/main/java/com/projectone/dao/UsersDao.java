package com.projectone.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.project.util.HibernateUtil;
import com.projectone.model.Reimbursement;
import com.projectone.model.Users;

public class UsersDao implements GenericDao<Users>{
	
	private HibernateUtil hUtil;
	
	public UsersDao() {
		// TODO Auto-generated constructor stub
	}
	
	public UsersDao(HibernateUtil hUtil) {
		super();
		this.hUtil = hUtil;
	}

	@Override
	public void insert(Users type) {
		Session ses = hUtil.getSession();
		Transaction tx = ses.beginTransaction();
		
		ses.save(type);
		tx.commit();
	}

	public Users selectById(int id) {
		Session ses = hUtil.getSession();
		
		Users user = ses.get(Users.class, id);
		return user;
	}
	
	public Users selectByUsername(String username) {
		Session ses = hUtil.getSession();
		
		List<Users> user = ses.createQuery("from Users where username ='" + username + "'", Users.class).list();
		
		if(user.size() == 0) {
			System.out.println("found no user with this username");
			return null;
		}
		return user.get(0);
	}
	
	public void updatePassword(int id, String password) {
		Session ses = hUtil.getSession();
		Transaction tx = ses.beginTransaction();

		Users user1 = (Users) ses.get(Users.class, id);
		user1.setPassword(password);
		
		ses.update(user1);
		tx.commit();
	}
	
}
