package com.projectone.dao;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.project.util.HibernateUtil;
import com.projectone.model.ReimbursementType;

public class ReimbursementTypeDao implements GenericDao<ReimbursementType>{

	private HibernateUtil hUtil;
	
	public ReimbursementTypeDao() {
		// TODO Auto-generated constructor stub
	}
	
	public ReimbursementTypeDao(HibernateUtil hUtil) {
		super();
		this.hUtil = hUtil;
	}

	@Override
	public void insert(ReimbursementType type) {
		Session ses = hUtil.getSession();
		Transaction tx = ses.beginTransaction();
		
		ses.save(type);
		tx.commit();
	}

	public void update(ReimbursementType type) {
		Session ses = hUtil.getSession();
		Transaction tx = ses.beginTransaction();
		
		ses.update(type);
		tx.commit();
	}

	@Override
	public ReimbursementType selectById(int id) {
		Session ses = hUtil.getSession();
				
		ReimbursementType reimbTyp = ses.get(ReimbursementType.class, id);
		return reimbTyp;
	}


}
