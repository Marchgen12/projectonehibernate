package com.projectone.dao;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.project.util.HibernateUtil;
import com.projectone.model.UserRoles;

public class UserRoleDao implements GenericDao<UserRoles>{

	private HibernateUtil hUtil;
	
	public UserRoleDao() {
		// TODO Auto-generated constructor stub
	}
	
	public UserRoleDao(HibernateUtil hUtil) {
		super();
		this.hUtil = hUtil;
	}

	@Override
	public void insert(UserRoles type) {
		Session ses = hUtil.getSession();
		Transaction tx = ses.beginTransaction();
		
		ses.save(type);
		tx.commit();
	}

	@Override
	public UserRoles selectById(int id) {
		Session ses = hUtil.getSession();
		
		UserRoles uRole = ses.get(UserRoles.class, id);
		return uRole;
	}

}
