package com.projectone.services;

import java.util.List;

import com.project.util.HibernateUtil;
import com.projectone.dao.UserRoleDao;
import com.projectone.dao.UsersDao;
import com.projectone.model.Reimbursement;
import com.projectone.model.UserRoles;
import com.projectone.model.Users;

public class UserServices {

	
	public void populateRole(HibernateUtil hUtil) {
		UsersDao uDao = new UsersDao(hUtil);
		UserRoleDao urDao = new UserRoleDao(hUtil);

		UserRoles ur1 = new UserRoles("Employeee");
		UserRoles ur2 = new UserRoles("Finance Manager");
		
		Users user1 = new Users("projectoneuser", "mypassword", "projectFirstName", "projectLastName", "pj1@gmail.com", 8);
		Users user2 = new Users("userTwo", "mypassword", "Roronoa", "Zoro", "googleMaps@gmail.com", 9);
		
		urDao.insert(ur1);
		urDao.insert(ur2);
		
		uDao.insert(user1);
		uDao.insert(user2);
	}
	
	private UsersDao uDao;
	
	public UserServices() {
		// TODO Auto-generated constructor stub
	}

	public UserServices(UsersDao uDao) {
		super();
		this.uDao = uDao;
	}
	
	public boolean Verification(String username, String password) {
		Users Login = uDao.selectByUsername(username);
		if(Login != null && Login.getPassword() == password) {
			return true;
		}else {
			System.out.println("Invalid Credential try again");
			return false;
		}
	}
	
	public Users getInfo(int id) {
		Users user = uDao.selectById(id);
		if(user != null) {
			return user;
		}
		return null;
	}
	
}
