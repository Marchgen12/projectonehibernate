package com.projectone.services;

import java.util.Date;
import java.util.List;

import com.project.util.HibernateUtil;
import com.projectone.dao.ReimbursementDao;
import com.projectone.dao.ReimbursementStatusDao;
import com.projectone.dao.ReimbursementTypeDao;
import com.projectone.dao.UsersDao;
import com.projectone.model.Reimbursement;
import com.projectone.model.ReimbursementStatus;
import com.projectone.model.ReimbursementType;
import com.projectone.model.Users;

public class ReimbursementServices {
	
	public void populateStatusRole(HibernateUtil hUtil) {
		ReimbursementStatusDao rsDao = new ReimbursementStatusDao(hUtil);
		ReimbursementTypeDao rtDao = new ReimbursementTypeDao(hUtil);
		
		ReimbursementStatus rs1 = new ReimbursementStatus("Pending");
		ReimbursementStatus rs2 = new ReimbursementStatus("Denied");
		ReimbursementStatus rs3 = new ReimbursementStatus("Approved");
		
		ReimbursementType rt1 = new ReimbursementType("Lodging");
		ReimbursementType rt2 = new ReimbursementType("Travel");
		ReimbursementType rt3 = new ReimbursementType("Food");
		ReimbursementType rt4 = new ReimbursementType("Others");
		
		rsDao.insert(rs1);
		rsDao.insert(rs2);
		rsDao.insert(rs3);
		
		rtDao.insert(rt1);
		rtDao.insert(rt2);
		rtDao.insert(rt3);
		rtDao.insert(rt4);
	}
	
	private ReimbursementDao rDao;
	private UsersDao uDao;
	
	public ReimbursementServices() {
		// TODO Auto-generated constructor stub
	}
	
	public ReimbursementServices(ReimbursementDao rDao) {
		super();
		this.rDao = rDao;
	}
	
	public Reimbursement getReimbursement(int id, String email) {
		Reimbursement reimb = rDao.selectById(id);
		if(reimb.getAuthorId().getUsername() == email){
			return reimb;	
		}
		return null;	
	}
	
	public List<Reimbursement> getReimbStatus(int Userid){
		List<Reimbursement> rList = rDao.getListReimbursements(Userid);
		return rList;
	}
	
	public void createNewReimbursement(int Amount, Date Submitted, String Description, String username, String password) {
		Users user = uDao.selectByUsername(username);
		if(user.getPassword() == password) {
			Reimbursement reimb = new Reimbursement(Amount, Submitted, Description, user);
			rDao.insert(reimb);
		}else {
			System.out.println("invalid credential");
		}
	}
}


